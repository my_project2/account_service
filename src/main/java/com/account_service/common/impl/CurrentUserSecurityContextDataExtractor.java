package com.account_service.common.impl;

import com.account_service.common.api.CurrentUser;
import com.account_service.common.api.DataExtractor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.UUID;

@Component
public class CurrentUserSecurityContextDataExtractor implements DataExtractor<CurrentUser> {

    private static final String ACCOUNT_ID = "account_id";
    private static final String USERNAME = "user_name";

    @Override
    public CurrentUser extractData() {
        Object details = SecurityContextHolder.getContext().getAuthentication().getDetails();
        Object decodedDetails = OAuth2AuthenticationDetails.class.cast(details).getDecodedDetails();
        Map map = Map.class.cast(decodedDetails);
        String accountId = String.class.cast(map.get(ACCOUNT_ID));
        String username = String.class.cast(map.get(USERNAME));
        return new CurrentUser(UUID.fromString(accountId), username);
    }

}
