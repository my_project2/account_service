package com.account_service.common.api;

public interface DataExtractor<T> {

    T extractData();

}
