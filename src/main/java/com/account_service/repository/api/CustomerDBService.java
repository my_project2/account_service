package com.account_service.repository.api;

import com.account_service.entity.account.Customer;

/**
 * Kostya Krivonos
 * 12/6/19
 * 4:09 PM
 */

public interface CustomerDBService {
    Customer save(Customer customer);
}
