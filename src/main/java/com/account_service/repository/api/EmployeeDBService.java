package com.account_service.repository.api;

import com.account_service.entity.account.Employee;

/**
 * Kostya Krivonos
 * 12/6/19
 * 4:09 PM
 */

public interface EmployeeDBService {

    Employee save(Employee employee);
}
