package com.account_service.repository.impl.employee;

import com.account_service.entity.account.Employee;
import com.account_service.repository.api.EmployeeDBService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * Kostya Krivonos
 * 12/6/19
 * 4:11 PM
 */

@Service
@RequiredArgsConstructor
public class EmployeeDBServiceImpl implements EmployeeDBService {
    private EmployeeRepository employeeRepository;

    public Employee save(Employee employee){
        return employeeRepository.save(employee);
    }


}
