package com.account_service.repository.impl.customer;

import com.account_service.entity.account.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Kostya Krivonos
 * 12/6/19
 * 4:01 PM
 */

@Repository
public interface AddressRepository extends JpaRepository<Employee, Long> {
}
