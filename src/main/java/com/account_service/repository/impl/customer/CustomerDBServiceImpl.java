package com.account_service.repository.impl.customer;

import com.account_service.entity.account.Customer;
import com.account_service.repository.api.CustomerDBService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * Kostya Krivonos
 * 12/6/19
 * 4:11 PM
 */

@Service
@RequiredArgsConstructor
public class CustomerDBServiceImpl implements CustomerDBService {
    private CustomerRepository customerRepository;

    @Override
    public Customer save(Customer customer){
        return customerRepository.save(customer);
    }
}
