//package com.account_service.configuration.security;
//
//import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
//import org.springframework.security.oauth2.common.OAuth2AccessToken;
//import org.springframework.security.oauth2.provider.OAuth2Authentication;
//import org.springframework.security.oauth2.provider.token.TokenEnhancer;
//
//import java.util.HashMap;
//import java.util.Map;
//
//public class AccountUuidTokenEnhancer implements TokenEnhancer {
//
//    @Override
//    public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
//        AccountDetails details = AccountDetails.class.cast(authentication.getPrincipal());
//        Map<String, Object> additionalInformation = new HashMap<>();
//        additionalInformation.put("account_id", details.getAccount().getId());
//        DefaultOAuth2AccessToken.class.cast(accessToken).setAdditionalInformation(additionalInformation);
//        return accessToken;
//    }
//
//}
