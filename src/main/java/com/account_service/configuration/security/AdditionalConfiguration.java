package com.account_service.configuration.security;

import com.account_service.configuration.security.resolver.CurrentUserHandlerMethodArgumentResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

@Configuration
public class AdditionalConfiguration implements WebMvcConfigurer {

    private final CurrentUserHandlerMethodArgumentResolver currentUserHandlerMethodArgumentResolver;

    @Autowired
    public AdditionalConfiguration(CurrentUserHandlerMethodArgumentResolver currentUserHandlerMethodArgumentResolver) {
        this.currentUserHandlerMethodArgumentResolver = currentUserHandlerMethodArgumentResolver;
    }

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> resolvers) {
        resolvers.add(currentUserHandlerMethodArgumentResolver);
    }

}
