package com.account_service.configuration.security.resolver;

import com.account_service.common.api.CurrentUser;
import com.account_service.common.api.DataExtractor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

@Component
public class CurrentUserHandlerMethodArgumentResolver implements HandlerMethodArgumentResolver {

    private final DataExtractor<CurrentUser> currentUserDataExtractor;

    @Autowired
    public CurrentUserHandlerMethodArgumentResolver(DataExtractor<CurrentUser> currentUserDataExtractor) {
        this.currentUserDataExtractor = currentUserDataExtractor;
    }

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return parameter.getParameterType().equals(CurrentUser.class);
    }

    @Override
    public Object resolveArgument(MethodParameter parameter,
                                  ModelAndViewContainer mavContainer,
                                  NativeWebRequest webRequest,
                                  WebDataBinderFactory binderFactory) throws Exception {
        return currentUserDataExtractor.extractData();
    }
}
