package com.account_service.configuration.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

@Configuration
@EnableResourceServer
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers(HttpMethod.POST, "/account/create").permitAll()
//                .antMatchers(HttpMethod.POST, "/facebook/login").permitAll()
//                .antMatchers(HttpMethod.GET, "/available").permitAll()
//                .antMatchers(HttpMethod.DELETE, "/password").permitAll()
//                .antMatchers(HttpMethod.POST, "/password").permitAll()
//                .antMatchers(HttpMethod.GET, "/ethereum/wallets").permitAll()
                .anyRequest().authenticated()
                .and().cors();
    }

}
