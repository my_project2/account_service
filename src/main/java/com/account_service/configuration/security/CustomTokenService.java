//package com.example.account.accountservice.configuration.security;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.oauth2.common.OAuth2AccessToken;
//import org.springframework.security.oauth2.common.util.OAuth2Utils;
//import org.springframework.security.oauth2.provider.ClientDetails;
//import org.springframework.security.oauth2.provider.ClientDetailsService;
//import org.springframework.security.oauth2.provider.OAuth2Authentication;
//import org.springframework.security.oauth2.provider.OAuth2Request;
//import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
//import org.springframework.security.oauth2.provider.token.TokenEnhancer;
//import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
//import org.springframework.stereotype.Service;
//
//import java.util.HashMap;
//import java.util.Set;
//
//import static com.skysoft.account.configuration.security.OAuth2Configuration.*;
//
///**
// * Kostya Krivonos
// * 2/7/19
// **/
//
//@Service
//public class CustomTokenService {
//    private final UserDetailsService userDetailsService;
//    private final DefaultTokenServices tokenServices;
//    private final JwtAccessTokenConverter jwtAccessTokenConverter;
//    private final Set<String> systemScope;
//
//    @Autowired
//    public CustomTokenService(DefaultTokenServices tokenServices, TokenEnhancer accountUuidTokenEnhancer, JwtAccessTokenConverter jwtAccessTokenConverter, UserDetailsService userDetailsService, @Qualifier("custom") ClientDetailsService clientDetailsService) {
//        this.userDetailsService = userDetailsService;
//        this.tokenServices = tokenServices;
//        this.jwtAccessTokenConverter = jwtAccessTokenConverter;
//        tokenServices.setTokenEnhancer(accountUuidTokenEnhancer);
//        ClientDetails systemClientDetails = clientDetailsService.loadClientByClientId(CLIENT_ID);
//        systemScope = systemClientDetails.getScope();
//    }
//
//    public OAuth2AccessToken createAccessToken(String username) {
//        HashMap<String, String> authorizationParameters = new HashMap<>();
//        authorizationParameters.put(OAuth2Utils.SCOPE, SCOPE_WEB_CLIENT);
//        authorizationParameters.put("username", username);
//        authorizationParameters.put(OAuth2Utils.CLIENT_ID, CLIENT_ID);
//        authorizationParameters.put(OAuth2Utils.GRANT_TYPE, GRANT_TYPE_PASSWORD);
//
//        UserDetails userDetails = userDetailsService.loadUserByUsername(username);
//        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(userDetails, username, userDetails.getAuthorities());
//        OAuth2Request oAuth2Request = new OAuth2Request(authorizationParameters, CLIENT_ID, userDetails.getAuthorities(),
//                true, systemScope, null, null, null, null);
//        OAuth2Authentication oAuth2Authentication = new OAuth2Authentication(oAuth2Request, authenticationToken);
//        OAuth2AccessToken accessToken = tokenServices.createAccessToken(oAuth2Authentication);
//        return jwtAccessTokenConverter.enhance(accessToken, oAuth2Authentication);
//    }
//}
