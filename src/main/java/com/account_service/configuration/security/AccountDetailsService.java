//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Primary;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.core.userdetails.UsernameNotFoundException;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//@Service
//@Primary
//public class AccountDetailsService implements UserDetailsService {
//
//    private final AccountRepository accountRepository;
//
//    @Autowired
//    public AccountDetailsService(AccountRepository accountRepository) {
//        this.accountRepository = accountRepository;
//    }
//
//    @Override
//    @Transactional
//    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//        return accountRepository.findByUsername(username)
//                .map(AccountDetails::new)
//                .orElseThrow(() -> new UsernameNotFoundException("User with provided username is not found"));
//    }
//}
