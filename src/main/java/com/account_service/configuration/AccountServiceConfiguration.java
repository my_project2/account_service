package com.account_service.configuration;

import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import java.util.Date;

@Configuration
public class AccountServiceConfiguration {

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }

    @Bean
    public ModelMapper modelMapper() {
        Converter<Date, Long> dateLongConverter = new AbstractConverter<Date, Long>() {
            protected Long convert(Date source) {
                return source == null ? null : source.toInstant().getEpochSecond();
            }
        };
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.addConverter(dateLongConverter);
        return modelMapper;
    }
}
