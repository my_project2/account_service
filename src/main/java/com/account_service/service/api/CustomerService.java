package com.account_service.service.api;

import com.account_service.entity.account.Customer;
import com.account_service.protocol.request.CreateCustomerRequest;

/**
 * Kostya Krivonos
 * 12/6/19
 * 4:04 PM
 */

public interface CustomerService {

    Customer create(CreateCustomerRequest request);
}
