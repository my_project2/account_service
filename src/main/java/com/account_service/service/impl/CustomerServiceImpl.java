package com.account_service.service.impl;

import com.account_service.entity.account.Customer;
import com.account_service.entity.account_details.CustomerAddress;
import com.account_service.protocol.request.CreateCustomerRequest;
import com.account_service.repository.api.CustomerDBService;
import com.account_service.service.api.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * Kostya Krivonos
 * 12/6/19
 * 4:06 PM
 */

@Service
@RequiredArgsConstructor
public class CustomerServiceImpl implements CustomerService {

    private CustomerDBService customerDBService;

    public Customer create(CreateCustomerRequest request){
        Customer customer = Customer.createCustomer(request);
        customer.setAddress(CustomerAddress.createAddress(request, customer));
        return customerDBService.save(customer);
    }

}
