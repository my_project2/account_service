package com.account_service.service.impl;

import com.account_service.entity.account.Employee;
import com.account_service.protocol.request.CreateEmployeeRequest;
import com.account_service.repository.api.EmployeeDBService;
import com.account_service.service.api.EmployeeService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * Kostya Krivonos
 * 12/6/19
 * 4:07 PM
 */

@Service
@RequiredArgsConstructor
public class EmployeeServiceImpl implements EmployeeService {
    private EmployeeDBService employeeDBService;

    public Employee create(CreateEmployeeRequest request){
        Employee employee = Employee.createCustomer(request);
        return employeeDBService.save(employee);
    }
}
