package com.account_service.protocol.request;

import com.account_service.entity.enums.RoleName;

/**
 * Kostya Krivonos
 * 12/6/19
 * 4:20 PM
 */

public class CreateEmployeeRequest {
    private String name;

    private String email;

    private RoleName roleName;

}
