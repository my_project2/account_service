package com.account_service.protocol.request;

import com.account_service.entity.account.Customer;
import com.account_service.entity.enums.RoleName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Kostya Krivonos
 * 12/6/19
 * 4:19 PM
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CreateCustomerRequest {

    private String name;

    private String secondName;

    private String email;

    private RoleName roleName;

    private String city;

    private String street;

    private String postIndex;

    private String phone;
}
