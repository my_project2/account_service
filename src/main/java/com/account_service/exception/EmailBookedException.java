package com.account_service.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class EmailBookedException extends RuntimeException {
    public EmailBookedException() {
        super("Provided email already booked");
    }
}
