package com.account_service.entity.account;

import com.account_service.entity.AuthorityEntity;
import com.account_service.entity.BaseEntity;
import com.account_service.entity.account_details.CustomerAddress;
import com.account_service.entity.enums.RoleName;
import com.account_service.protocol.request.CreateCustomerRequest;
import lombok.*;

import javax.persistence.*;

/**
 * Kostya Krivonos
 * 12/6/19
 * 2:24 PM
 */

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Customer extends BaseEntity {

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String secondName;

    @Column(nullable = false)
    private String email;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private RoleName roleName;

    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "address_id", nullable = false)
    private CustomerAddress address;

    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "authority_id", nullable = false)
    private AuthorityEntity authority;

    public static Customer createCustomer(CreateCustomerRequest request) {
        return Customer.builder()
                .name(request.getName())
                .secondName(request.getSecondName())
                .email(request.getEmail())
                .roleName(request.getRoleName())
                .build();
    }
}
