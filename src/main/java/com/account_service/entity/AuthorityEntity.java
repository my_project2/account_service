package com.account_service.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@Entity(name = "user_authorities")
@EqualsAndHashCode(callSuper = true)
public class AuthorityEntity extends BaseEntity {

    private boolean authorized;
    private boolean emailSend;
    private int attemptCount;
    private Instant stopLockTime;
    private Instant lastLoginTime;

    @Column(nullable = false)
    private String password;

    public AuthorityEntity() {
//        this.stopLockTime = Instant.now();
        this.password = UUID.randomUUID().toString();
    }

    public boolean isLocked() {
        return stopLockTime.isAfter(Instant.now());
    }

    public void updateStopLockTime(Integer attemptCount, Integer lockMinutes) {
        this.attemptCount = attemptCount;
        this.authorized = false;
        this.stopLockTime = Instant.now().plus(lockMinutes, ChronoUnit.MINUTES);

    }

}
