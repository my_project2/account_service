package com.account_service.entity;

import com.account_service.entity.account.Employee;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.UUID;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "password_token")
@EqualsAndHashCode(callSuper = true)
@NamedEntityGraph(name = "userWithPassToken", attributeNodes = @NamedAttributeNode(value = "user"))
public class PasswordTokenEntity extends BaseEntity {

    @Column(nullable = false)
    private String token;

    @Column(nullable = false)
    private Instant expirationTime;

    @OneToOne(optional = false, fetch = FetchType.LAZY)
    private Employee employee;

    public PasswordTokenEntity(Long minutesToExpPassToken, Employee employee) {
        this.token = UUID.randomUUID().toString();
        this.expirationTime = Instant.now().plus(minutesToExpPassToken, ChronoUnit.MINUTES);
        this.employee = employee;
    }

    public void updateAndGenerateNewToken(Long minutesToExpPassToken) {
        this.token = UUID.randomUUID().toString();
        this.expirationTime = Instant.now().plus(minutesToExpPassToken, ChronoUnit.MINUTES);
    }

}
