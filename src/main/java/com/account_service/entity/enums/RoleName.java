package com.account_service.entity.enums;

/**
 * Kostya Krivonos
 * 12/6/19
 * 2:28 PM
 */

public enum RoleName {
    ROLE_EMPLOYEE, ROLE_CUSTOMER, ROLE_ADMIN
}
