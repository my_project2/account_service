package com.account_service.entity.account_details;

import com.account_service.entity.account.Customer;
import com.account_service.protocol.request.CreateCustomerRequest;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Kostya Krivonos
 * 12/6/19
 * 3:49 PM
 */

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CustomerAddress {
    @Column
    private String city;
    @Column
    private String street;
    @Column
    private String postIndex;
    @Column
    private String phone;

    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "customer_id", nullable = false)
    private Customer customer;

    public static CustomerAddress createAddress(CreateCustomerRequest request, Customer customer){
        return CustomerAddress.builder()
                .city(request.getCity())
                .street(request.getStreet())
                .postIndex(request.getPostIndex())
                .phone(request.getPhone())
                .customer(customer)
                .build();
    }
}
